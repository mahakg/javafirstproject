import java.util.ArrayList;


public class Node {
	private ArrayList<Node> childrens = null;
	private String value;
	
	Node(String value){
		this.childrens = new ArrayList<>();
		this.value = value;
	}
	
	public void addChild(Node child){
		childrens.add(child);
	}
	
}
